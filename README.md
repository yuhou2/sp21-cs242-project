# Music Search and Library
This Spotify Music Search App using Spotify Music API. You can search for albums and playlists. You should login to your own Spotify account first and get an access to the API. Then youare ready for the website. The website displays the details with a beautiful UI and you can add and maintain separate load more functionality for Albums and Artists.

# Available Scripts
Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.
In the project directory, you can run:
- yarn install
- yarn start


# Programming Language and Platform
JavaScript, React, Redux

