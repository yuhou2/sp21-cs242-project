const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const auth = require('../../music-search-library-app/middleware/auth');
const config = require('config');
const { check, validationResult } = require('express-validator');
const User = require('../models/user');

// @route    POST api/user
// @desc     Register user
// @access   Public
router.post(
  '/',
  check('username', 'Required username').notEmpty(),
  check('email', 'Invalid email').isEmail(),
  check('password', 'Password should longer than 6').isLength({ min: 6 }),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { username, email, password } = req.body;

    try {
      let user = await User.findOne({ email });

      if (user) {
        return res
          .status(400)
          .json({ errors: 'User already existed' });
      }

      user = new User({
        username,
        email,
        password
      });

      const salt = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, salt);

      await user.save();

      const payload = {
        user: {
          id: user.id
        }
      };

      jwt.sign(
        payload,
        config.get('jwtSecret'),
        { expiresIn: '10 days' },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server error');
    }
  }
);


// @route    GET api/user
// @desc     Get user profile by user ID
// @access   Public
router.get('/me', auth,
    async (req, res) => {
      try {
        const profile = await User.findOne({
          _id: req.user.id
        }).populate('user', ['username', 'password']);
  
        if (!profile) return res.status(400).json({ errors: 'User not found' });
  
        return res.json(profile);
      } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
      }
    }
);
  

// @route    DELETE api/profile
// @desc     Delete profile, user & posts
// @access   Private
router.delete('/me', auth, async (req, res) => {
    try {
      // Remove user
      await Promise.all([
        User.findOneAndRemove({ _id: req.user.id })
      ]);
  
      res.json({ msg: 'User deleted' });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
});

// @route    PUT api/password
// @desc     Update user password
// @access   Private
router.put('/me/password', [auth,
    check('password', 'Password is required').notEmpty()],
    async (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
  
      try {
        const user = await User.findOne({ _id: req.user.id });
        const salt = await bcrypt.genSalt(10);
        const { password } = req.body;
        user.password = await bcrypt.hash(password, salt);
        await user.save();
        res.json(user);
      } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
      }
    }
);


// @route    PUT api/username
// @desc     Update user username
// @access   Private
router.put('/me/username', [auth,
    check('username', 'Username is required').notEmpty()],
    async (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
  
      try {
        const user = await User.findOne({ _id: req.user.id });
        // console.log(user)
        const { username } = req.body;
        // console.log(req.body)
        user.username = username;
        // console.log(req.body.username)
        // console.log(username)
        await user.save();
        res.json(user);
      } catch (err) {
        console.log(req.body.username)
        console.error(err.message);
        res.status(500).send('Server Error');
      }
    }
);



module.exports = router;