/* eslint-disable no-undef */
const express = require('express')
const router = express.Router()
const Library = require('../models/library')
const { body, validationResult } = require('express-validator');


// @route    GET api/library/me
// @desc     Get current library profile
// @access   Private
router.get('/alllibrary', async (req, res) => {
    try {
        const collection = await Library.find()
        res.json(collection)
        if (!collection) {
            return res.status(400).json({ msg: 'There is no library for this user' });
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});

// @route    GET /library/me
// @desc     Get current users library
// @access   Private
router.get('/:email', getLibrary, (req, res) => {
    res.json(res.library)
})

// @route    Post /library/
// @desc     Post one library
// @access   Public 
router.post('/', body('email').isEmail(), async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const libraryFields = {
            email: req.body.email,
            albums: req.body.albums
        }
        try {
            // Using upsert option (creates new doc if no match is found):
            const oneLibrary = await Library.findOneAndUpdate(
                { email: req.body.email },
                { $set: libraryFields },
                { new: true, upsert: true, setDefaultsOnInsert: true }
            );
            return res.json(oneLibrary);
        } catch (err) {
            res.status(400).json({ message: err.message })
        }
})


// @route    Patch api/library/
// @desc     Update one library
// @access   Public 
router.put('/:email', getLibrary, body().notEmpty(), async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    try {
        console.log(res.library.email)
        req.body.forEach(function (item) {
            res.library.albums.unshift(item)
        })
        const updatedLibrary = await res.library.save()
        res.json(updatedLibrary)
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

// @route    Delete api/library/me
// @desc     Delete one library
// @access   Public
router.delete('/:email', getLibrary, async (req, res) => {
    try {
        await res.library.remove()
        res.json({ message: 'Deleted This Library' })
    } catch(err) {
        res.status(500).json({ message: err.message })
    }
})

// @route    DELETE api/library/:email/albums/:id
// @desc     Delete one album from library base on id
// @access   Public

router.delete('/:email/albums/:id', getLibrary, async (req, res) => {
    try {

        res.library.albums = res.library.albums.filter(
            (albums) => albums.id.toString() !== req.params.id
        );

        await res.library.save();

        return res.status(200).json(res.library);
    } catch (error) {
        console.error(error);
        return res.status(500).json({ msg: 'Server error' });
    }
});

// @route    DELETE api/library/:email/images/:image_id
// @desc     Delete one image from library base on image_id
// @access   Public

router.delete('/:email/albums', getLibrary, async (req, res) => {
    try {
        req.body.albums.forEach(function (item) {
            res.library.albums = res.library.albums.filter(
                (albums) => albums.id.toString() !== item.id.toString()
            );

        })


        await res.library.save();

        return res.status(200).json(res.library);
    } catch (error) {
        console.error(error);
        return res.status(500).json({ msg: 'Server error' });
    }
});




async function getLibrary(req, res, next) {
    try {
      const library = await Library.findOne({'email': {'$regex': req.params.email}})
      if (library == null) {
        return res.status(404).json({ message: 'Cant find library'})
      }
      res.library = library
        next()
    } catch(err){
      return res.status(500).json({ message: err.message })
    }
}

module.exports = router