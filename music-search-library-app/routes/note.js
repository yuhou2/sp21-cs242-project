/* eslint-disable no-undef */
const express = require('express')
const router = express.Router()
const Note = require('../models/note')
const { body, validationResult } = require('express-validator');


// @route    GET api/note/me
// @desc     Get current note profile
// @access   Private
router.get('/allnote', async (req, res) => {
    try {
        const note = await Note.find()
        res.json(note)
        if (!note) {
            return res.status(400).json({ msg: 'There is no note for this album' });
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});

// @route    GET /note/me
// @desc     Get current users note
// @access   Private
router.get('/:album', getNote, (req, res) => {
    res.json(res.note)
})

// @route    Post /note/
// @desc     Post one note
// @access   Public 
router.post('/', async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const noteFields = {
            album: req.body.album,
            notes: req.body.notes
        }
        try {
            // Using upsert option (creates new doc if no match is found):
            const oneNote = await Note.findOneAndUpdate(
                { album: req.body.album },
                { $set: noteFields },
                { new: true, upsert: true, setDefaultsOnInsert: true }
            );
            return res.json(oneNote);
        } catch (err) {
            res.status(400).json({ message: err.message })
        }
})


// @route    Patch api/note/
// @desc     Update one note
// @access   Public 
router.put('/:album', getNote, body().notEmpty(), async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    try {
        req.body.forEach(function (item) {
            res.note.notes.unshift(item)
        })
        const updatedNote = await res.note.save()
        res.json(updatedNote)
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

// @route    Patch api/note/noteid
// @desc     Update one note
// @access   Public 
router.put('/:album/:noteid', getNote, async (req, res) => {
    try {
        res.note.notes.forEach((element, index) => {
            if (element.id === req.params.noteid) {
                res.note.notes[index].note = req.body.note;
            }
        })
        const updatedNote = await res.note.save()
        res.json(updatedNote)
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

// @route    Delete api/note/me
// @desc     Delete one note
// @access   Public
router.delete('/:album', getNote, async (req, res) => {
    try {
        await res.note.remove()
        res.json({ message: 'Deleted This Note' })
    } catch(err) {
        res.status(500).json({ message: err.message })
    }
})

async function getNote(req, res, next) {
    try {
      const note = await Note.findOne({'album': {'$regex': req.params.album}})
      if (note == null) {
        return res.status(404).json({ message: 'Cant find note'})
      }
      res.note = note
        next()
    } catch(err){
      return res.status(500).json({ message: err.message })
    }
}

module.exports = router