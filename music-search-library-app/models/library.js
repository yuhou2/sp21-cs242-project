const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const librarySchema = new Schema({
    email: {
        type: String,
    },
    albums: [
        {
            id: {
                type: String,
            },
            img_src: {
                type: String,
            }
        }
    ],
},
    { collection: 'library' }
);

module.exports = mongoose.model('library', librarySchema);