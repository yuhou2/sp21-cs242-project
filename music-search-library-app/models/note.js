const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const noteSchema = new Schema({
    album: {
        type: String,
    },
    notes: [
        {
            username: {
                type: String,
            },
            note: {
                type: String,
            }
        }
    ],
},
    { collection: 'note' }
);

module.exports = mongoose.model('note', noteSchema);