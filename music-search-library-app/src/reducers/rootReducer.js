import { combineReducers } from 'redux';
import album from './albums';
import alert from './alert';
import artists from './artists';
import auth from './auth';

export default combineReducers({
  alert,
  auth,
  album,
  artists
});