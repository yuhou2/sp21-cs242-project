import axios from 'axios';

const get = async function (url, params) {
    try {
        const params = JSON.parse(localStorage.getItem('access'));
        if (params) {
            axios.defaults.headers.common['Authorization'] = `Bearer ${params.access_token}`;
        }
    } catch (error) {
        console.log('Error setting auth', error);
    }
    const result = await axios.get(url, params);
    return result.data;
};

export const addAlbums = (albums) => ({
    type: 'ADD_ALBUMS',
    albums
});
  
export const addArtists = (artists) => ({
    type: 'ADD_ARTISTS',
    artists
});
  
export function getResultFromAPI(searchTerm) {
    return async (dispatch) => {
        try {
            const API_URL = `https://api.spotify.com/v1/search?query=${encodeURIComponent(
                searchTerm
            )}&type=album,artist`;
            const result = await get(API_URL);
            console.log(result);
            const { albums, artists } = result;
            dispatch({type:'SET_ALBUMS', albums});
            return dispatch({type:'SET_ARTISTS', artists});
        } catch (error) {
            console.log('error', error);
        }
    };
}

export function loadMoreAlbums(url) {
    return async (dispatch) => {
        try {
            const result = await get(url);
            return dispatch(addAlbums(result.albums));
        } catch (error) {
            console.log('error', error);
        }
    };
}

export function loadMoreArtists(url) {
    return async (dispatch) => {
        try {
            const result = await get(url);
            return dispatch(addArtists(result.artists));
        } catch (error) {
            console.log('error', error);
        }
    };
}

