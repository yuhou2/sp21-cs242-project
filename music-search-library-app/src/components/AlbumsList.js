import React from 'react';
import { Card } from 'react-bootstrap';
import _ from 'lodash';
import music from '../images/music.jpeg';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { loadUser } from '../actions/auth';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIconOutlined from '@material-ui/icons/FavoriteBorderOutlined';
import Favorited from '@material-ui/icons/Favorite';

const AlbumsList = ({ albums, auth: { user, isAuthenticated }, loadUser }) => {
  async function onChangeFavorite() {
    if (isAuthenticated) {
      const imageSaveList = [{"id":albums.items[0].id, "img_src":albums.items[0].images[0].url}];
      const res = await fetch(
        "/library/"+ user.email, {
        method: 'Put',
  
        headers: { 'Content-Type': 'application/json' },
        
        body: JSON.stringify(imageSaveList),
      }
  
      );
      const data = await res.json();
      console.log(data)
    } else {
      alert("You need to log in")
      return <Redirect to="/login" />;
    }
    
  }
    return (
      <React.Fragment>
        {Object.keys(albums).length > 0 && (
          <div className="albums">
            {albums.items.map((album, index) => (
              <React.Fragment key={index}>
                <Card style={{ width: '18rem' }}>
                  <a
                    target="_blank"
                    href={album.external_urls.spotify}
                    rel="noopener noreferrer"
                    className="card-image-link"
                  >
                    {!_.isEmpty(album.images) ? (
                      <Card.Img
                        variant="top"
                        src={album.images[0].url}
                        alt="" />
                    ) : (
                      <img src={music} alt="" />
                    )}
                  </a>
                  <Card.Header>{album.name}</Card.Header>
                  <Card.Body>
                    <Card.Title>{album.artists.map((artist) => artist.name).join(', ')}</Card.Title>
                    <Card.Text>
                      <small>
                        Release Date: {album.release_date}
                        {"\n"}
                        Total Tracks: {album.total_tracks}
                      </small>
                    </Card.Text>
                  </Card.Body>
                    <div style={{display: 'flex', flexDirection: 'row', alignItems:'center'}}>
                        <IconButton aria-label="add to favorites" onClick={onChangeFavorite}>
                            {<FavoriteIconOutlined/>}
                        </IconButton>
                    </div>
                    <b style={{marginLeft: 'auto', marginRight: '25px', fontSize: "20px"}}></b>
                </Card>
              </React.Fragment>
            ))}
          </div>
        )}
      </React.Fragment>
    );
};
// export default AlbumsList;

AlbumsList.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
  auth: state.auth
});


export default connect(mapStateToProps, { loadUser })(AlbumsList);