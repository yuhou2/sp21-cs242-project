import Gallery from 'react-grid-gallery';
import { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import { deepPurple } from '@material-ui/core/colors';
import NavigationBar from './NavigationBar';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { loadUser } from '../actions/auth';
import { Link, Redirect } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({

    button: {
        minWidth: 100,
        background: 'white',
        color: deepPurple[500],
        fontWeight: 300,
        borderStyle: 'none',
        borderWidth: 2,
        borderRadius: 12,
        paddingLeft: 14,
        paddingTop: 14,
        paddingBottom: 15,
        boxShadow: '0px 5px 8px -3px rgba(0,0,0,0.14)',
        "&:focus": {
            borderRadius: 12,
            background: 'white',
            borderColor: deepPurple[500]
        },
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },

}));

function UserLibrary({ auth: { user }, loadUser }) {


    const [imagesList, setImageList] = useState([])
    const email = user.email;
    const classes = useStyles();



    function setData(data) {
        var tempList = [];
        if (data == null) {
            alert("there is no album");
            return <Redirect to="/dashboard" />;
        }
        data.forEach(function (entry) {
            var singObj = {}
            singObj['src'] = entry.img_src
            singObj['thumbnail'] = entry.img_src
            singObj['thumbnailWidth'] = 320
            singObj['thumbnailHeight'] = 320
            singObj['id'] = entry.id
            tempList.push(singObj)
        });
        setImageList(tempList)
        console.log(imagesList)
    }
    function openDetails(id) {
        var image = imagesList[id]
        console.log(image)
    }
    function onSelectImage(index, image) {
        var images = this.state.images.slice();
        var img = images[index];
        if (img.hasOwnProperty("isSelected"))
            img.isSelected = !img.isSelected;
        else
            img.isSelected = true;

        this.setState({
            images: images
        });
    }
    const handleDeleteImage = (event) => {
        event.preventDefault();
        let seletimageList = []
        imagesList.forEach(function (entry) {
            if (entry.hasOwnProperty("isSelected")) {
                var singObj = {}
                singObj['id'] = entry.id
                seletimageList.push(singObj)
            }
        });
        const imageDeleteList =
        {
            'albums': seletimageList
        }
        console.log(imageDeleteList)
        fetchPhoto();


        async function fetchPhoto() {
            const res = await fetch(

                "/library/"+ email + "/albums", {
                method: 'Delete',

                headers: { 'Content-Type': 'application/json' },

                body: JSON.stringify(imageDeleteList),
            }

            );
            const data = await res.json();
            console.log(data)
        }
        window.location.reload();


    };

    useEffect(() => {
        fetchPhoto();

        async function fetchPhoto() {
            const res = await fetch(
                `/library/${email} `
            );
            const data = await res.json();
            setData(data[['albums']])
        }

    }, []);
    return (
        <div>
            <NavigationBar />
            <div style={{
                paddingTop: 50,
                paddingLeft: 200,
                paddingRight: 200,
                display: "block",
                overflow: "auto",
                minHeight: "1px",
                width: "100%",
            }}>
                <Gallery images={imagesList} showLightboxThumbnails onSelectImage={onSelectImage} />
            </div>
            <div style={{ paddingTop: 20 }}>
                <button className={classes.button} onClick={handleDeleteImage}> Delete </button>
            </div>
        </div>
    )

}

UserLibrary.propTypes = {
    auth: PropTypes.object.isRequired
};
  
const mapStateToProps = (state) => ({
    auth: state.auth
});


export default connect(mapStateToProps, { loadUser })(UserLibrary);