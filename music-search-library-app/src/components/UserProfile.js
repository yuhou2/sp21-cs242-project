import {
    Avatar,
    Box,
    Button,
    Card,
    CardActions,
    CardContent,
    Divider,
    Typography,
    Grid,
    TextField,
    CardHeader,

} from '@material-ui/core';
import { useState, useEffect, } from "react";
import NavigationBar from './NavigationBar';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { loadUser } from '../actions/auth';


export let useremail = '';


function UserProfile({ auth: { token }, loadUser }, props) {

    // const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNjA3ZDg3OGYxYzI3MDllZjNmYWQzMzEyIn0sImlhdCI6MTYxODg1MjU4NywiZXhwIjoxNjE5NzE2NTg3fQ.wxm10UAHjNtRP77EkeQO3ETml4n_Cs9EZ7yMwICi9eE"
    // console.log(auth.token)
    // const token = auth.token;
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('token', token);
    

    const [user, setUser] = useState('')
    const [values, setValues] = useState({ username: '', password: '' });


    const handleChange = (event) => {

        setValues({
            ...values,
            [event.target.name]: event.target.value
        });
    };


    async function getData(url, methods, headers, bodys) {

        const res = await fetch(url, {
            method: methods,
            headers: headers,
            body: bodys
        }
        );
        const data = await res.json()
        setUser(data)
        useremail = data.email
    }
    // }
    const handleSumbit = (evt) => {
        evt.preventDefault();
        if (values.username) {
            getData('/user/me/username', 'PUT', myHeaders, JSON.stringify({ 'username': values.username }))
            alert("name change success")
        }
        if (values.password) {
            getData('/user/me/password', 'PUT', myHeaders, JSON.stringify({ 'password': values.password }))
            alert("password change success")
        }

    }

    useEffect(() => {
        getData('/user/me', 'GET', myHeaders, null)
    }, []);


    return (

        <div>
            <NavigationBar />
            <div>
                <Card {...props}>
                    <CardContent>
                        <Box
                            sx={{
                                alignItems: 'center',
                                display: 'flex',
                                flexDirection: 'column'
                            }}
                        >
                            <Typography
                                color="textPrimary"
                                gutterBottom
                                variant="h3"
                            >
                                {user.username}
                            </Typography>
                            <Typography
                                color="textSecondary"
                                variant="body1"
                            >
                                {`${user.email}`}
                            </Typography>
                            
                        </Box>
                    </CardContent>
                    <Divider />
                </Card>

            </div>
            <div>
                <form
                    autoComplete="off"
                    noValidate
                    {...props}
                >
                    <Card>
                        <CardHeader
                            subheader="The information can be edited"
                            title="Profile"
                        />
                        <Divider />
                        <CardContent>
                            <Grid
                                container
                                spacing={2}
                            >
                                <Grid
                                    item
                                    md={4}
                                    xs={12}
                                >
                                    <TextField
                                        fullWidth
                                        label="New Username"
                                        name="username"
                                        onChange={handleChange}
                                        value={values.username}
                                        variant="outlined"
                                    />
                                </Grid>



                                <Grid
                                    item
                                    md={4}
                                    xs={12}
                                >
                                    <TextField
                                        fullWidth
                                        label="New Password"
                                        name="password"
                                        onChange={handleChange}
                                        value={values.state}
                                        variant="outlined"
                                    >
                                    </TextField>
                                </Grid>
                            </Grid>
                        </CardContent>
                        <Divider />
                        <Box
                            sx={{
                                display: 'flex',
                                justifyContent: 'flex-end',
                                p: 2
                            }}
                        >
                            <Button
                                color="primary"
                                variant="contained"
                                onClick={handleSumbit}
                            >
                                Save details
                            </Button>
                        </Box>
                    </Card>
                </form>
            </div>
        </div>
    );


}

UserProfile.propTypes = {
    auth: PropTypes.object.isRequired
};
  
const mapStateToProps = (state) => ({
    auth: state.auth
});


export default connect(mapStateToProps, { loadUser })(UserProfile);