import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
const SearchForm = (props) => {
  const [searchTerm, setSearchTerm] = useState('');
  const [errorMsg, setErrorMsg] = useState('');
  function handleInputChange(event) {
        const searchValue = event.target.value;
        setSearchTerm(searchValue);
    }
  function search(event) {
        event.preventDefault();
        if (searchTerm.trim() !== '') {
            props.getSearchResult(searchTerm);
        } else {
            setErrorMsg("You didn't enter anything. Please enter what you want to search.");
        }
    }
  return (
    <div className="top-search">
      <Form onSubmit={search}>
        {errorMsg && <p className="errorMsg">{errorMsg}</p>}
        <Form.Group controlId="formBasicEmail">
          <Form.Control
            type="search"
            name="searchTerm"
            value={searchTerm}
            placeholder="Anything you want to search about albums and artists"
            onChange={handleInputChange}
            autoComplete="off"
          />
        </Form.Group>
        <Button variant="info" type="submit">
          Submit
        </Button>
      </Form>
    </div>
  );
};
export default SearchForm;