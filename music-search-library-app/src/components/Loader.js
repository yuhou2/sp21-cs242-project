import { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';

function Loader(props) {
    const [node] = useState(document.createElement('div'));
    const ld = document.querySelector('#loader');

    useEffect(() => {
        ld.appendChild(node).classList.add('message');
    }, [ld, node]);

    useEffect(() => {
        if (!props.show) {
            ld.classList.add('hide');
            document.body.classList.remove('loader-open');
        } else {
            ld.classList.remove('hide');
            document.body.classList.add('loader-open');
        }
    }, [ld, props.show]);

    return ReactDOM.createPortal(props.children, node);
}
export default Loader;