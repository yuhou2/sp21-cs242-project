const express = require('express');
const mongoose = require('mongoose');
const url = 'mongodb+srv://yuneshou:caomeiwei@spotify.vlsp0.mongodb.net/music?retryWrites=true&w=majority';
const connectDB = async () => {
    try {
        await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('MongoDB Connected...');
    } catch (err) {
        console.error(err.message);
        process.exit(1)
    }
}
const app = express();

//Connect Database
connectDB();

app.get('/api', (req, res) => res.send('Its working!'));

app.use(express.json())

const userRouter = require('./routes/user')
app.use('/user', userRouter)
const libraryRouter = require('./routes/library')
app.use('/library', libraryRouter)
const noteRouter = require('./routes/note')
app.use('/note', noteRouter)
app.use('/auth', require('./routes/auth'));

app.listen(4000, function () {
    console.log('now listening for requests');
});