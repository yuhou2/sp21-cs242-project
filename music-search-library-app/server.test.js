const mongoose = require("mongoose")
const Library = require('../music-search-library-app/models/library')
const Note = require('../music-search-library-app/models/note')
const User = require('../music-search-library-app/models/user')
const supertest = require("supertest");
const url = 'mongodb+srv://yuneshou:caomeiwei@spotify.vlsp0.mongodb.net/music?retryWrites=true&w=majority';
const express = require('express');

beforeEach((done) => {
	mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, () => done());
})

afterEach((done) => {
    mongoose.connection.close(() => done());
});

function createServer() {
    const app = express();
    app.get('/api', (req, res) => res.send('Its working!'));
    app.use(express.json())
    const userRouter = require('./routes/user')
    app.use('/user', userRouter)
    const libraryRouter = require('./routes/library')
    app.use('/library', libraryRouter)
    const noteRouter = require('./routes/note')
    app.use('/note', noteRouter)
    return app;
}

const app = createServer();

// test 1
test("GET /library", async () => {
    const lib = {
      email: "5242342@gmail.com",
      albums: [{
        id: "3RDqXDc1bAETps54MSSOW0",
        img_src: "https://i.scdn.co/image/ab67616d0000b2738cffb7c6c40759eaf8a5a142"}],
    }
  
    await supertest(app)
      .get("/library/5242342@gmail.com")
      .expect(200)
      .then((response) => {
        expect(response.body.email).toBe(lib.email);
        expect(response.body.albums.id).toBe(lib.albums.id);
        expect(response.body.albums.img_src).toBe(lib.albums.img_src);
      });
  });

// test 2
test("GET /library/:email", async () => {
	const lib = {
        email: "2353453@gmail.com",
        albums: [{
          id: "3RDqXDc1bAETps54MSSOW0",
          img_src: "https://i.scdn.co/image/ab67616d0000b2738cffb7c6c40759eaf8a5a142"}],
    }

	await supertest(app)
		.get("/library/" + lib.email)
		.expect(200)
		.then((response) => {
            expect(response.body.email).toBe(lib.email);
            expect(response.body.albums.id).toBe(lib.albums.id);
            expect(response.body.albums.img_src).toBe(lib.albums.img_src);
		})
})

// test 3
test("POST /library", async () => {
	const lib = {
        email: "435345@gmail.com",
        albums: [{
          id: "3RDqXDc1bAETps54MSSOW0",
          img_src: "https://i.scdn.co/image/ab67616d0000b2738cffb7c6c40759eaf8a5a142"}],
    }

	await supertest(app)
		.post("/library")
		.send(lib)
		.expect(200)
		.then(async (response) => {
			// Check the data in the database
			const res = await Library.findOne({ _id: response.body._id })
			expect(res).toBeTruthy();
            expect(res.email).toBe(lib.email);
		})
})

// test 4
test("PUT /library", async () => {
    const lib = {
        email: "435345@gmail.com",
        albums: [{
          id: "3RDqXDc1bAETps54MSSOW0",
          img_src: "https://i.scdn.co/image/ab67616d0000b2738cffb7c6c40759eaf8a5a142"}],
    }

    const newlib = [{
          id: "3RDqXDc1bAETps54MSSOW1",
          img_src: "https://i.scdn.co/image/ab67616d0000b2738cffb7c6c40759eaf8a5a142"}]

  
    await supertest(app)
      .put("/library/" + lib.email)
      .send(newlib)
      .expect(200)
      .then(async (response) => {
        // Check the data in the database
        const newLib = await Library.findOne({ _id: response.body._id });
        expect(newLib).toBeTruthy();
      });
  });

// test 5
test("DELETE /api/posts/:id", async () => {
    const lib = await Library.create({
        email: "422242@gmail.com",
        albums: [{
          id: "3RDqXDc1bAETps54MSSOW0",
          img_src: "https://i.scdn.co/image/ab67616d0000b2738cffb7c6c40759eaf8a5a142"}],
    });
  
    await supertest(app)
      .delete("/library/" + lib.email)
      .expect(200)
      .then(async () => {
        expect(await Library.findOne({ _id: lib.id })).toBeFalsy();
      });
  });


// test 6
test("GET /library/alllibrary", async () => {
    await supertest(app)
    .get("/library/alllibrary")
    .expect(200)
    .then((response) => {
        expect(Array.isArray(response.body)).toBeTruthy();
    });
});




// test 7
test("GET /note", async () => {
    const note = {"album":"4X1RoVYHgqkUT3ylEQewUd","notes":[{"username":"yuneshou","note":"temp note 2"}]}
  
    await supertest(app)
      .get("/note/4X1RoVYHgqkUT3ylEQewUd")
      .expect(200)
      .then((response) => {
        expect(response.body.album).toBe(note.album);
        expect(response.body.notes.username).toBe(note.notes.username);
        expect(response.body.notes.note).toBe(note.notes.note);
      });
  });

// test 8
test("GET /note/:album", async () => {
	const note = {"album":"4X1RoVYHgqkUT3ylEQewUd","notes":[{"username":"yuneshou","note":"temp note 2"}]}

	await supertest(app)
		.get("/note/" + note.album)
		.expect(200)
		.then((response) => {
            expect(response.body.album).toBe(note.album);
            expect(response.body.notes.username).toBe(note.notes.username);
            expect(response.body.notes.note).toBe(note.notes.note);
		})
})

// test 9
test("POST /note", async () => {
	const note = {"album":"3uxyNfYxJXAWbXsf2fRNBr","notes":[{"username":"tony","note":"temp note 3"}]}

	await supertest(app)
		.post("/note")
		.send(note)
		.expect(200)
		.then(async (response) => {
			// Check the data in the database
			const res = await Note.findOne({ _id: response.body._id })
			expect(res).toBeTruthy();
            expect(res.album).toBe(note.album);
		})
})

// test 10
test("PUT /note", async () => {
    const note = {"album":"3uxyNfYxJXAWbXsf2fRNBr","notes":[{"username":"tony","note":"temp note 3"}]}

    const newNote = [{"username":"tony2","note":"temp note 4"}]

  
    await supertest(app)
      .put("/note/" + note.album)
      .send(newNote)
      .expect(200)
      .then(async (response) => {
        // Check the data in the database
        const res = await Note.findOne({ _id: response.body._id });
        expect(res).toBeTruthy();
      });
  });

// test 11
test("DELETE /note/:album", async () => {
    const note = await Note.create({"album":"4TEkdmWgOV6ys0LJjcpAiV","notes":[{"username":"tzhu","note":"temp note tzhu"}]});
  
    await supertest(app)
      .delete("/note/" + note.album)
      .expect(200)
      .then(async () => {
        expect(await Library.findOne({ _id: note.id })).toBeFalsy();
      });
  });


// test 12
test("GET /note/allnote", async () => {
    await supertest(app)
    .get("/note/allnote")
    .expect(200)
    .then((response) => {
        expect(Array.isArray(response.body)).toBeTruthy();
    });
});

// test 13
test("PUT /note/album", async () => {
    const newNote = {"note":"note test 13"}
    await supertest(app)
      .put("/note/3uxyNfYxJXAWbXsf2fRNBr/6073e21a02c469e7b8211a73")
      .send(newNote)
      .expect(200)
      .then(async (response) => {
        // Check the data in the database
        const res = await Note.findOne({ _id: response.body._id });
        expect(res).toBeTruthy();
      });
});




// // test 14
// test("GET /user", async () => {
//     const user = {"email":"yhou316@gmail.com","password":"832795423865","username":"lol"}
  
//     await supertest(app)
//       .get("/user/yhou316@gmail.com")
//       .expect(200)
//       .then((response) => {
//         expect(response.body.email).toBe(user.email);
//         expect(response.body.username).toBe(user.username);
//         expect(response.body.password).toBe(user.password);
//       });
//   });

// // test 15
// test("GET /user/:email", async () => {
// 	const user = {"email":"yhou316@gmail.com","password":"832795423865","username":"lol"}

// 	await supertest(app)
// 		.get("/user/" + user.email)
// 		.expect(200)
// 		.then((response) => {
//             expect(response.body.email).toBe(user.email);
//             expect(response.body.username).toBe(user.username);
//             expect(response.body.password).toBe(user.password);
// 		})
// })

// // test 16
// test("POST /user", async () => {
// 	const user = {"email":"tony@gmail.com","password":"75682735","username":"lol457683"}

// 	await supertest(app)
// 		.post("/user")
// 		.send(user)
// 		.expect(200)
// 		.then(async (response) => {
// 			// Check the data in the database
// 			const res = await User.findOne({ _id: response.body._id })
// 			expect(res).toBeTruthy();
//             expect(res.email).toBe(user.email);
// 		})
// })

// // test 17
// test("PUT /user", async () => {
//     const user = {"email":"tony@gmail.com","password":"75682735","username":"lol457683"}

//     const newUser = {"password":"74562852","username":"457385"}

  
//     await supertest(app)
//       .put("/user/" + user.email)
//       .send(newUser)
//       .expect(200)
//       .then(async (response) => {
//         // Check the data in the database
//         const res = await User.findOne({ _id: response.body._id });
//         expect(res).toBeTruthy();
//       });
//   });

// // test 18
// test("DELETE /api/user/:email", async () => {
//     const user = await User.create({"email":"lulu@gmail.com","password":"37468535","username":"lol56433"})
  
//     await supertest(app)
//       .delete("/user/" + user.email)
//       .expect(200)
//       .then(async () => {
//         expect(await User.findOne({ _id: user.id })).toBeFalsy();
//       });
//   });

// // test 19
// test("DELETE /user/:email", async () => {
//   const user = await User.create({"email":"lulu@gmail.com","password":"37468535","username":"lol56433"})

//   await supertest(app)
//     .delete("/user/" + user.email)
//     .expect(200)
//     .then(async () => {
//       expect(await User.findOne({ _id: user.id })).toBeFalsy();
//     });
// });


// // test 20
// test("GET /user/alluser", async () => {
//     await supertest(app)
//     .get("/user/alluser")
//     .expect(200)
//     .then((response) => {
//         expect(Array.isArray(response.body)).toBeTruthy();
//     });
// });
