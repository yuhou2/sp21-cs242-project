# A Webpage for Music Search and Library
Yu Hou (yuhou2) | Moderator: Changcheng Fu (cf7)

This is a project for CS242 which is a music website that help people to find artists, albums, playlists.

## Abstract
### Project Purpose
This is a website about music. It will have the latest information about music. This includes artists, albums, playlists, release dates, etc. Here people can find their favorite songs and artists.

### Project Motivation
Because I personally enjoy listening to songs in my free time, many music sites have a lot of information and require payment to access the information. What I have created is a simple, easy website where you can find your favorite artist or song in a very clear and concise way.

## Technical Specification
- Platform: Webpage app (React)
- Programming Languages: JavaScript (React for frontend, Express for backend)
- Stylistic Conventions: Airbnb JavaScript Style Guide
- SDK: Facebook SDK for React
- IDE: Visual Studio Code
- Tools/Interfaces: Webpage
- Target Audience: Broad-range audience, especially music lovers

## Functional Specification
### Features
- List view with search and sort functionality for music details API
- Music cover gallery view, API details -
- Detailed view of the item
- User login page, user library containing a user's personal main and photography pages 
- Important message and message page -


## Brief Timeline
- Week 1: Search view creation, users can search their favoriate songs and can select the music genre to be more specific.
- Week 2: Backend for user and database collection, create the user and library database, add API to them so that front end can access them.
- Week 3: User and collection database frontend creation, users can log in and sign up an account to save their favorate songs.
- Week 4: Make the web attractive and add more detailed message.

## Grading Calculator of Each Week's Rubric
https://drive.google.com/file/d/1is_Lu4dq91tnxtqha9ZHx1HozeO3FxLJ/view?usp=sharing

## Rubrics
### Week 1
| Category  | Total Score Allocated | Detailed Rubrics                                                            |
|-----------|:---------:|-------------------------------------------------------------------------------|
|  List View |  5  |  0: didn't implement anything <br> +1: list view display relevant items  <br> +2: render music as a list <br> +1: display bigger image as clicking <br> +1: display images with information provided |
|  Search Query & Sort |  8  |  0: Didn't implement anything <br> +2: query for field filters <br> +2: query for keyword matching <br> +2: query for number of songs displayed <br> +1: rendering notification of errors <br> +1: search bar filter down the items based on the search |
| Genre select |  2  |  0: didn't implement anything <br> +2: implemented music type selection|
|  Manual test plan for List View  |  5  |  0: Didn't implement tests <br> for every 2 manual test, gain 1 point
|  Manual test plan for Query and Search |  5  |  0: Didn't implement tests <br> for every 2 manual test, gain 1 point


### Week 2
| Category  | Total Score Allocated | Detailed Rubrics                                                            |
|-----------|:---------:|-------------------------------------------------------------------------------|
|  API for user |  7  |  0: didn't implement anything <br> +1: implemented GET API for account information <br> +2: implemented POST API for add a new user <br> +2: implemented DELETE API for delete a user account <br> +2: implemented PUT API for update user favorite songs information/account information |
|  API for library |  8  |  0: didn't implement anything <br> +1: implemented GET API for search query <br> +1: implemented GET API for getting songs' information for given ID <br> +2: implemented POST API for add new songs <br> +2: implemented DELETE API fro delete songs <br> +2: implemented PUT API to update the songs information(i.e. songs pubish dates, singers, creators)|
|  Unit Test for user |  5  |  0: Didn't implement tests <br> for every 2 unit tests, gain 1 point |
|  Unit Test for library |  5  |  0: Didn't implement tests <br> for every 2 unit tests, gain 1 point |

### Week 3
| Category  | Total Score Allocated | Detailed Rubrics                                                            |
|-----------|:---------:|-------------------------------------------------------------------------------|
| Sign up page |  3 |  0: didn't implement anything <br> +1: correctly making post request <br> +2: render notification |
| Log in page |  3  |  0: Didn't implement anything <br> +1: correctly check the login information <br> +1: render notification <br> +1: redirect to main page |
| Profile page |  5  |  0: Didn't implement anything <br> +1.5: successfully and neatly display user information <br> +1.5: correctly update user information <br> +2: can successfully delete the user then logout |
| Library page |  4  |  0: Didn't implement anything <br> +2: display as a list view <br> +1: successfully add items <br> +1: successfully delete items |
| Manual test plan for webpage |  10  |  0: Didn't implement tests <br> for every 2 manual tests, gain 1 point |

### Week 4
| Category  | Total Score Allocated | Detailed Rubrics                                                            |
|-----------|:---------:|-------------------------------------------------------------------------------|
|  Notes render |  5  |  0: Didn't implement anything <br> +2: successfully render notes as clicking the songs <br> +1: successfully add notes <br> +1: successfully delete notes <br> +1: successfully update notes |
|  Message API |  6  |  0: Didn't implement anything <br> +2: implemented GET API <br> +2: implemented POST API <br> +2: implemented DELETE API |
|  Message Page |  2  |  0: Didn't implement anything  <br> +1: correctly delete messages <br> +1: implemented notification alert |
|  More Attractive Webpage |  2  |  0: Didn't implement anything <br> +2: the user interface is beautiful (at least with logo and background) |
|  Unit Test for API |  5  |  0: Didn't implement tests  <br> for every 2 unit tests, gain 1 point |
|  Manual test plan for webpage |  5  |  0: Didn't implement tests  <br> for every 2 unit tests, gain 1 point |
